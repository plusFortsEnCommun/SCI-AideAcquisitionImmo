# SCI open source et aide à l'accession à la propriété

# Présentation du projet

Une SCI est une [société civile immobilière](https://fr.wikipedia.org/wiki/Soci%C3%A9t%C3%A9_civile_immobili%C3%A8re).

Ce type de société peut être créé par un **acte sous seing privé** ou par un **acte authentique** et permet l'achat de parts sur un ou plusieurs biens immobiliers.

Sans vendre les biens immobiliers en eux-même, il est donc possible d'aquérir/vendre des parts ou bien encore, d'en faire un don manuel à ses enfants/petits-enfants pour alléger une future succession.

Des loyers peuvent être collectés et répartis entre les sociétaires au prorata des parts détenues par chacun.

Des charges peuvent également être réparties selon cette même méthode.

Dans le contexte actuel:
- solidité et  santé des banques remise en question, 
- coûts de élevés des assurances emprunteurs malgré des taux bas
- durées d'emprunts dépassant souvent les 20 ans
- difficultés croissantes pour trouver un emploi à temps plein et donc un logement,

j'ai pensé qu'il était temps d'utiliser ce que permet la loi pour aider mon prochain.

L'idée est de créer une **SCI** dont le but sera d'aider un des **sociétaires** à devenir **propriétaire** du bien immobilier tout en assurant aux autres co-acquéreurs de part la sécurité financière et le sentiment d'**aider son prochain à prendre son autonomie**.

Nous appellerons par la suite le sociétaire appelé à devenir propriétaire le **sociétaire "acquéreur"** et nous appellerons par la suite les sociétaires qui aident le **sociétaire "acquéreur"** à acquérir les parts restantes de la SCI des **sociétaires "prêteurs"**

Pour synthétiser, le but est d'**acheter à plusieurs** un **bien immobilier**, si possible sans emprunt, via une **SCI**, pour que celle-ci le **loue à prix bas** au **sociétaire "acquéreur"** jusqu'au rachat final de toutes les parts restantes par celui-ci . 

Le respect d'un échéancier pré-établi de rachat de parts par l'ensemble des parties prenantes à la création de la structure juridique est ici important.

Le **degré d'altruisme des sociétaires** peut être: 
- totalement désintéressé et le sociétaire "acquéreur" ne paie qu'un loyer annuel correspondant aux charges, impôts et provisions éventuelles pour travaux d'entretien ou d'aménagement du bien

=> implique peu d'impacts fiscaux sur les sociétaires acquéreurs ou autres.
- une aide bienveillante mais avec un léger taux d'emprunt où les sociétaires louent le bien au sociétaire acquéreur avec une rémunération du prêt et donc à un prix légèrement supérieur aux seuls charges, impôts et provisions mineures pour travaux d'entretien ou d'aménagement 

 => implique un impact fiscal modéré sur les sociétaires acquéreurs ou autres.
- une approche classique où la SCI loue le bien au sociétaire "acquéreur" à un prix de marché 

 => peu intéressant car impliquerait des impacts fiscaux chez tous les sociétaires mais encore plus chez le sociétaire "acquéreur" qui devrait payer un impôt sur le revenu pour une partie du loyer pourtant déboursés de sa poche, partie proportionnelle aux parts détenues par lui ... A simuler donc... 

Pour faire cela, il faudra bien évidement lire les textes de lois, trouver un fonctionnement équilibré entre les sociétaires via des processus de décision et un statut solide et prévoir les points de paramétrage selon les niveaux d'altruisme souhaités entre les parties prenantes.

**Le résultat de ces travaux étant destinés à être proposés à la manière des projets open source en accès libre et gratuit pour tous !**

# Appel à compétences

Etant architecte informatique,je ne connais pas tous les domaines nécessaires à la finalisation de ce projet. 

Aussi, je cherche dès aujourd'hui l'aide de toute personne maîtrisant en tout ou partie:

- Le droit fiscal pour lister l'ensemble des avantages ou risques fiscaux que cette technique pourrait apporter
- Le droit immobilier pour lister l'ensemble des avantages ou risques liés aux SCIs et à la propriété partagée
- La compatbilité pour faire des simulations et montrer les différences entre achat et placement classiques par rapport à la solution envisagée
- Le droit en contrat des sociétés civiles immobilières pour rédiger des statuts solides et adaptables à différents degrés de solidarité.
- L'altruisme et l'envie de donner de son énergie et de son temps ;-)

Pour soumettre votre candidature, n'hésitez pas à modifier cette page pour ajouter à la section qui suit vos noms et prénoms si vous le souhaitez ainsi que votre pseudonyme et vos domaines de compétence.

Nom et Prénom|Pseudonyme|Domaines de compétence
---------------------|----------------|---------------------------------
Tony Blanchard|PlusFortsEnCommun|Architecte/développeur informatique et hacker social

# Fonctionnement

Ce répertoire **Framagit** sera partagé en **modification** avec tous les participants inscrits sur le site Framagit avant publication sur un repository ouvert
Chaque participant pourra déposer :
- **des fichiers pour chaque proposition de paragraphe des statuts** de SCI avec, à l'intérieur, un ou plusieurs arguments documentés et des références documentaires précises
- **des questions en vrac** sur la page **Questions-Réponses.md** du répertoire **EnConstruction**
- **des réponses** documentées et sourcées à des questions de la page **Questions-Réponses.md** du répertoire **EnConstruction**

Au fur et à mesure de l'enrichissement, le document de travail PropositionDeStatuts.md du répertoire EnConstruction sera complété par mes soins, au départ de cette aventure, avec prise en comptes des avis et compétences des personnes ayant fait des propositions. Un système de vote sera mis en place en cours de travail.

Des réunions dématérialisées pourront avoir lieu entre les différents participants selon répartition géographique. 

Des réunions physiques pourront également avoir lieu.

Dans l'idéal, ces réunions devraient avoir lieu tous les mois durant l'élaboration des différentes propositions de statuts.

# Notes

Attention, l'utilisation des ces statuts devra être faite **sous votre seule et entière responsabilité**, ce projet n'est qu'une aide bénévole pour tenter de résoudre un problème donné au moment de sa rédaction et **doit être validé juridiquement** avant mise en oeuvre par vos soins.
 