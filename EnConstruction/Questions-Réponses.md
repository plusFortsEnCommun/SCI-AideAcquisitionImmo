# Questions

Voici quelques exemples de questions auquelles il faudra répondre pour créer des statuts équilibrés

## Une SCI a-t-elle le droit de louer son bien à l'un des sociétaires ?

## Une SCI a-t-elle le droit de louer son bien à prix coûtant (foncier, charges, frais de gestion de la SCI) ?

## Comment organiser le souhait d'un sociétaire "prêteur" de se retirer de la SCI ?

Il faut prévoir des cas de sorti des sociétaires en cas de nécessité de récupérer sa mise...

Il faudrait également prévoir que ceci ne déséquilibre pas le fonctionnement global de la SCI...

Ex:
- Si le sortant souhaite vendre ses parts à une personne extérieure à la SCI, quels sont les règles à fixer ?
-    Si le sortant souhaite vendre à un autre sociétaire "prêteur" ? Y a -t-il risque de prise de pouvoir sur la SCI ? Comment le prévenir ? Quel prix doit-il appliquer le prix d'achat initial, le prix du marché estimé ?
-    Si le sortant souhaite vendre au sociétaire acquéreur, quel prix doit-il appliquer le prix d'achat initial, le prix du marché estimé ?

_          NB: Le problème de la prise de pouvoir est moins gênante ici puisque le bien est destiné au sociétaire acquéreur...Néanmoins, que se passe-t-il si avec du pouvoir, le sociétaire "acquéreur" refuse d'entretenir le bien et donc fait baisser son prix de marché ? 

          ==> Quel prix doit-il appliquer le prix d'achat initial, le prix du marché estimé ? Doit-on bloquer le prix des parts sur le prix d'achats ?_

## Comment doit-être pris en compte le revenu locatif dans l'impôt sur le revenu des sociétaires ?

   Est-il possible de simuler les impacts sur l'imposition de chacun

## Si la SCI loue son bien au sociétaire "acquéreur", celui-ci paie un loyer et une part de celui-ci lui est reversé au prorata de ses parts de SCI comme un revenu ... Est-ce que ce montages est plus intéressant qu'un achat classique en terme d'équivalent TEG ?

## Que faut-il prévoir en cas de décès/invalidité du sociétaire acquéreur ? 

   Les autres sociétaires ont-ils le droit de sortir de la SCI ? 

   ....


A suivre (viens d'être initié)
